'use strict'

const Blog = require('./app/controllers/blog');
const Assets = require('./app/controllers/assets');
const Accounts = require('./app/controllers/accounts');
const PublicBlogs = require('./app/controllers/publicblogs');
const Administrator = require('./app/controllers/administrator');

module.exports = [

  { method: 'GET', path: '/', config: Accounts.main },
  { method: 'GET', path: '/signup', config: Accounts.signup },
  { method: 'GET', path: '/login', config: Accounts.login },
  { method: 'GET', path: '/logout', config: Accounts.logout },
  { method: 'POST', path: '/register', config: Accounts.register },
  { method: 'POST', path: '/authenticate', config: Accounts.authenticate },
  { method: 'GET', path: '/postblog', config: Blog.postblog },
  { method: 'POST', path: '/postBlog', config: Blog.postBlog },
  { method: 'POST', path: '/deleteSelectedBlogs', config: Blog.deleteSelectedBlogs },
  { method: 'GET', path: '/settings', config: Accounts.viewSettings },
  { method: 'POST', path: '/updateSettings', config: Accounts.updateSettings },
  { method: 'GET', path: '/publicblogs', config: PublicBlogs.publicblogs },
  { method: 'POST', path: '/selectBlogger', config: PublicBlogs.selectBlogger },
  { method: 'GET', path: '/admin', config: Administrator.admin },
  { method: 'POST', path: '/deleteUserSelectedBlogs', config: Administrator.deleteUserSelectedBlogs },
  { method: 'GET', path: '/profile', config: Accounts.profile },
  { method: 'POST', path: '/getprofile', config: Accounts.getprofile },
  { method: 'POST', path: '/follow', config: Accounts.follow },
  { method: 'POST', path: '/unfollow', config: Accounts.unfollow },
  { method: 'POST', path: '/deleteUser', config: Administrator.deleteUser },
  { method: 'POST', path: '/adminRegister', config: Administrator.adminRegister },
  { method: 'GET', path: '/getImage/{_id}', config: Accounts.getImage },
  { method: 'POST', path: '/uploadImage', config: Accounts.uploadImage },
  { method: 'GET', path: '/getBlogImage/{_id}', config: Blog.getBlogImage },
  { method: 'GET', path: '/Error', config: Accounts.error },
  { method: 'GET', path: '/adminstats', config: Administrator.stats },

  {
    method: 'GET',
    path: '/{param*}',
    config: { auth: false },
    handler: Assets.servePublicDirectory,
  },

];
