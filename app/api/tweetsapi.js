'use strict';

const Tweet = require('../models/tweet');
const Boom = require('boom');
const ByteArray = require('bytearray');

//handler to return all tweets on request, altered to return image data as base64 string to android
exports.find = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.find({}).populate('user').exec().then(tweets => {
      for (let x of tweets) {
        if (x.img.data != null) {
          x.base64String = new String(x.img.data.toString('base64'));
        }
      }
      reply(tweets);
      console.log(tweets);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

//handler to find and return tweet by id
exports.findOne = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.findOne({ _id: request.params.id }).then(tweet => {
      if (tweet != null) {
        reply(tweet);
      } else {
        reply(Boom.notFound('id not found'));
      }
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};

//handler to create tweet and return, using the sent byte array from android if it exists
exports.create = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    console.log(request.payload);
    const tweet = new Tweet(request.payload);
    console.log('phto: '+request.payload.photoByte);
    if(request.payload.photoByte != null){
      tweet.img.data = request.payload.photoByte;
      tweet.img.contentType = String;
    }
    tweet.save().then(newTweet => {
      Tweet.findOne({ _id: newTweet._id }).populate('user').then(newTweet1 => {
        console.log(newTweet1);
        reply(newTweet1).code(201);
      });
    }).catch(err => {
      console.log('error');
      reply(Boom.badImplementation('error creating Tweet'));
    });
  },

};

//handler to delete tweets on android request
exports.deleteOne = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.remove({ _id: request.params.id }).then(tweet => {
      reply(tweet).code(204);
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};

//handler to delete all tweets
exports.deleteAll = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing users'));
    });
  },

};

