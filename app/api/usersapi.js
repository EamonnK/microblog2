'use strict';
const utils = require('./utils.js');
const User = require('../models/user');
const Tweet = require('../models/tweet');
const Boom = require('boom');
const Bcrypt = require('bcrypt-nodejs');

//handler to find all and return users
exports.find = {

  //auth: false,
  auth: {
    strategy: 'jwt',
  },
  handler: function (request, reply) {
    User.find({}).exec().then(users => {
      reply(users);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

//handler to find and return one user
exports.findOne = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.findOne({ _id: request.params.id }).then(user => {
      if (user != null) {
        reply(user);
      } else {
        reply(Boom.notFound('id not found'));
      }
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};

//handler to create user, encrypt their password for storage in the database, returning user to
//android with unencrypted password
exports.create = {

  auth: false,

  handler: function (request, reply) {
    console.log(request.payload);
    const user = new User(request.payload);
    user.password = Bcrypt.hashSync(user.password);
    user.save().then(newUser => {
      newUser.password = request.payload.password;
      reply(newUser).code(201);
    }).catch(err => {
      console.log('error');
      reply(Boom.badImplementation('error creating User'));
    });
  },

};

//handler for deleting user
exports.deleteOne = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.remove({ _id: request.params.id }).then(user => {
      reply(user).code(204);
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};

//handler to delete all users
exports.deleteAll = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    User.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing tweets'));
    });
  },

};

//handler to delete all tweets of a single user by id
exports.deleteUserTweets = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Tweet.remove({ user: request.params.id }).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Tweets'));
    });
  },

};

//handler to authenticate a users login from android, creating a token with the foundUser
//and replying the token to the application
exports.authenticate = {

  auth: false,

  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({ email: user.email }).then(foundUser => {
      if (foundUser && Bcrypt.compareSync(user.password, foundUser.password)) {
        const token = utils.createToken(foundUser);
        foundUser.password = user.password;
        console.log('ok');
        reply({ success: true, token: token, user: foundUser }).code(201);
      } else {
        console.log('er1');
        reply({ success: false, message: 'Authentication failed. User not found.' }).code(201);
      }
    }).catch(err => {
      console.log('err2');
      reply(Boom.notFound('internal db failure'));
    });
  },

};

//handler to establish a follows relationship from the android app
exports.follow = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const follower = request.payload;
    const followed = request.params.id;
    User.findOne({ _id: followed }).then(targetUser => {
      User.findOne({ _id: follower }).then(sourceUser => {
        targetUser.followedBy.push(sourceUser._id);
        sourceUser.follows.push(targetUser._id);
        targetUser.save();
        sourceUser.save();
        reply(sourceUser).code(201);
      });
    }).catch(err => {
      reply(Boom.badImplementation('error following'));
    });
  },

};

//handler to remove follows relationship
exports.unfollow = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const follower = request.payload;
    const followed = request.params.id;
    User.findOne({ _id: followed }).then(targetUser => {
      User.findOne({ _id: follower }).then(sourceUser => {
        targetUser.followedBy.remove(sourceUser._id);
        sourceUser.follows.remove(targetUser._id);
        targetUser.save();
        sourceUser.save();
        reply(sourceUser).code(201);
      });
    }).catch(err => {
      reply(Boom.badImplementation('error unfollowing'));
    });
  },

};
