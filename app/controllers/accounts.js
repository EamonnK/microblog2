'use strict'

const User = require('../models/user');
const Tweet = require('../models/tweet');
const Joi = require('joi');
const Bcrypt = require('bcrypt-nodejs');

//handler for main hbs, replying with view
exports.main = {
  auth: false,
  handler: (request, reply) => {
    reply.view('main');
  },

};

//handler for main hbs, replying with view
exports.error = {
  auth: false,
  handler: (request, reply) => {
    reply.view('Error');
  },

};

//handler for logout route
exports.logout = {
  auth: false,
  handler: function (request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/');
  },
};

//handler for signup route, rendering signup.hbs
exports.signup = {
  auth: false,
  handler: function (request, reply) {
    reply.view('signup');
  },

};

//handler replying with login.hbs
exports.login = {
  auth: false,
  handler: function (request, reply) {
    reply.view('login');
  },

};

//handler for register post route, validating form has been filled correctly
//and creating a new user if so, saving to database
exports.register = {
  auth: false,

  validate: {

    payload: {
      firstName: Joi.string().required().regex(/^[a-zA-Z]{2,}$/).options({
        language: {
          string: {
            regex: {
              base: 'must be letters only and at least 2 letters long!',
            },
          },
        },
      }),
      lastName: Joi.string().min(2).required().regex(/^[a-zA-Z]{2,}$/).options({
        language: {
          string: {
            regex: {
              base: 'must be letters only and at least 2 letters long!',
            },
          },
        },
      }),
      email: Joi.string().email().required(),
      password: Joi.string().regex(/^(?=.*[0-9]){1}(?=.*[A-Z]){1}([a-zA-Z0-9]){8,}$/).required().options({
        language: {
          string: {
            regex: {
              base: ' must contain at least one number, one capital Letter and be at least 8 characters long!',
            },
          },
        },
      }),
    },

    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },

  },

  handler: function (request, reply) {
    User.findOne({ email: request.payload.email }).then(existingUser => {
      if (existingUser === null) {
        const user = new User(request.payload);
        user.password = Bcrypt.hashSync(user.password);
        user.save().then(newUser => {
          reply.redirect('/login');
        });
      } else {
        let message = 'Email already exists.';
        reply.view('signup', { existingUser: message });
      }
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//hanlder for authenticate post route, allowing for admin sign in or otherwise
//checking input credentials against users in the database, if found replying with their home page
//if not, replying with signup.hbs
exports.authenticate = {
  auth: false,

  validate: {

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('login', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },

  },

  handler: function (request, reply) {
    const user = request.payload;

    User.findOne({ email: user.email }).then(foundUser => {
      if (user.email === 'admin@admin.com' && user.password === 'secret') {
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: 'admin@admin.com',
        });
        reply.redirect('/admin');
      }else if (foundUser && Bcrypt.compareSync(user.password, foundUser.password)) {
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: user.email,
        });
        reply.redirect('/postblog');
      } else {
        reply.redirect('/signup');
      }
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for viewsettings route, finding the logged in user and rendering their details to the
// page
exports.viewSettings = {

  handler: function (request, reply) {
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).populate('follows').populate('followedBy').then(foundUser => {
      Tweet.find({ user: foundUser._id }).then(userTweets => {
        let tweetslength = userTweets.length;
        reply.view('settings', { title: 'Edit Account Settings', user: foundUser,
          numberTweets: tweetslength, current: foundUser, });
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for rendering the profile page with the users details. also for rendering
// a target users profile page with their details and the id of the user that requested the page.
exports.getprofile = {
  handler: function (request, reply) {
    var finduser = request.payload._id;
    let userEmail = request.auth.credentials.loggedInUser;
    let tweets = [];
    User.findOne({ _id: finduser }).populate('follows').populate('followedBy').then(foundUser => {
      foundUser.follows.push(foundUser._id);
      for (let x of foundUser.follows) {
        Tweet.find({ user: x._id }).populate('user').then(friendsTweets => {
          for (let y of friendsTweets) {
            tweets.push(y);
          }
        });
      }

      foundUser.follows.pop(foundUser._id);
      Tweet.find({ user: foundUser._id }).then(userTweets => {
        let tweetslength = userTweets.length;
        User.findOne({ email: userEmail }).populate('follows').populate('followedBy').then(current => {
          tweets.reverse();
          reply.view('profile', { target: foundUser,
            numberTweets: tweetslength, follows: foundUser.follows,
            followedBy: foundUser.followedBy,
            current: current, tweets: tweets,
          });
        });
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for follows, adding target id to users follows array and users id to targets followedBy
exports.follow = {
  handler: function (request, reply, err) {
    const follower = request.payload.source;
    const followed = request.payload.target;
    User.findOne({ _id: followed }).then(targetUser => {
      User.findOne({ _id: follower }).then(sourceUser => {
        for (let x of targetUser.followedBy) {
          if (x.equals(sourceUser._id) || sourceUser === targetUser) {
            throw err;
          }
        }

        targetUser.followedBy.push(sourceUser._id);
        sourceUser.follows.push(targetUser._id);
        targetUser.save();
        sourceUser.save();
        reply.view('profile', { user: targetUser, follows: targetUser.follows,
          followedBy: targetUser.followedBy, current: sourceUser, });
      });
    }).catch(err => {
      reply.view('profile');
    });
  },

};

//handler for unfollow method, removing id from users follows and targets followedBy array
exports.unfollow = {
  handler: function (request, reply, err) {
    const unfollower = request.payload.source;
    const followed = request.payload.target;
    User.findOne({ _id: followed }).then(targetUser => {
      User.findOne({ _id: unfollower }).then(sourceUser => {
        for (let x of targetUser.followedBy) {
          if (!(x.equals(sourceUser._id))) {
            throw err;
          }
        }

        targetUser.followedBy.remove(sourceUser._id);
        sourceUser.follows.remove(targetUser._id);
        targetUser.save();
        sourceUser.save();
        reply.view('profile', { user: targetUser, follows: targetUser.follows,
          followedBy: targetUser.followedBy, current: sourceUser, });
      });
    }).catch(err => {
      reply.view('profile');
    });
  },

};

//handler to render profile page
exports.profile = {
  auth: false,
  handler: function (request, reply) {
    reply.view('profile');
  },

};

//handler for updatesettings route, validating the form and making specified changes
exports.updateSettings = {

  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('settings', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },
  },
  handler: function (request, reply) {
    const update = request.payload;
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      if (update.password === user.password) {
        user.email = (update.email === '') ? userEmail.email : update.email;
        user.firstName = (update.firstName === '') ? userEmail.firstName : update.firstName;
        user.lastName = (update.lastName === '') ? userEmail.lastName : update.lastName;
      }

      return user.save();
    }).then(user => {
      reply.view('settings', { title: 'Edit Account Settings', user: user });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for upload image route, taking in payload from form, getting the logged in user
//finding by email, and initializing the users img field with appropriate data
exports.uploadImage = {

  handler: function (request, reply) {
    const data = request.payload.picture;
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      user.img.data = data;
      user.img.contentType = String;
      return user.save();
    }).then(user => {
      reply.view('settings', { current: user });
    }).catch(err => {
      reply.redirect('settings');
    });
  },

};

//handler for getimage route, taking passed param from page, finding the user ,
//and replying with their image
exports.getImage = {

  handler: function (request, reply) {
    User.findOne({ _id: request.params._id }).then(user => {
      reply(user.img.data).type('image');
    });
  },

};

