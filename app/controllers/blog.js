'use strict'

const User = require('../models/user');
const Tweet = require('../models/tweet');

//Handler for user home page, rendering their list of blogs if any
exports.postblog = {
  handler: function (request, reply) {
    const userEmail = request.auth.credentials.loggedInUser;
    let user;
    User.findOne({ email: userEmail }).then(gotUser => {
      user = gotUser;
      return Tweet.find({ user: gotUser._id });
    }).then(userTweets => {
      userTweets.reverse();
      reply.view('postblog', { tweets: userTweets, current: user });
    }).catch(err => {
      reply.redirect('postblog');
    });
  },

};

//handler for postblog route, adding user to blog field and  saving to database
exports.postBlog = {
  handler: function (request, reply) {
    const tweetData = request.payload;
    const picture = request.payload.img;
    const userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(user => {
      tweetData.user = user._id;
      const tweet = new Tweet(tweetData);
      if (picture.buffer !== undefined) {
        tweet.img.data = picture;
        tweet.img.contentType = String;
      }

      return tweet.save();
    }).then(newTweet => {
      reply.redirect('postblog');
    }).catch(err => {
      reply.redirect('postblog');
    });
  },

};

//handler for delete blog route, deleting selected blogs handling whether it is an individual
//blog or an array, or if selection is empty
exports.deleteSelectedBlogs = {
  handler: function (request, reply) {
    const data = request.payload;
    if (data !== undefined) {
      if (Array.isArray(data._id)) {
        const range = data._id.length;
        for (let i = 0; i < range; i++) {
          Tweet.findOne({ _id: data._id[i] }).then(deldata => {
            return Tweet.remove(deldata);
          }).catch(err => {
            reply.redirect('postblog');
          });
        }
      } else {
        Tweet.findOne({ _id: data._id }).then(deldata => {
          return Tweet.remove(deldata);
        }).catch(err => {
          reply.redirect('postblog');
        });
      }
    }

    reply.redirect('postblog');
  },

};

//handler for getimage route, taking passed param from page, finding the user ,
//and replying with their image
exports.getBlogImage = {

  handler: function (request, reply) {
    Tweet.findOne({ _id: request.params._id }).then(tweet => {
      reply(tweet.img.data).type('image');
    });
  },

};

