'use strict'

const User = require('../models/user');
const Tweet = require('../models/tweet');
const AdminPage = require('../views/admin');

//handler for admin route, rendering admin home page, posting users in filter dropdown
//all users in delete dropdown, all blogs in table
exports.admin = {

  handler: function (request, reply) {
    let postingUser = [];
    let allUsers = [];
    Tweet.find({}).populate('user').then(allTweets => {
      for (let us of allTweets) {
        postingUser.push(us.user);
      }

      const uniqueUsers = postingUser.filter(function (elem, index, self) {
        return index == self.indexOf(elem);
      });

      User.find({}).then(users => {
        for (let user of users) {
          allUsers.push(user);
        }

        allTweets.reverse();
        return allUsers;
      }).then(allUsers => {
        reply.view('admin', {
          title: 'User Tweets',
          tweets: allTweets,
          user: uniqueUsers,
          allUsers: allUsers,
        });
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

//handler for deleting blogs from admin page, dealing with individual, array or empty entry.
exports.deleteUserSelectedBlogs = {
  handler: function (request, reply) {
    const data = request.payload;
    if (data !== undefined) {
      if (Array.isArray(data._id)) {
        const range = data._id.length;
        for (let i = 0; i < range; i++) {
          Tweet.findOne({ _id: data._id[i] }).populate('user').then(deldata => {
            return Tweet.remove(deldata);
          }).catch(err => {
            reply.redirect('admin');
          });
        }
      } else {
        Tweet.findOne({ _id: data._id }).then(deldata => {
          return Tweet.remove(deldata);
        }).catch(err => {
          reply.redirect('admin');
        });
      }
    }

    reply.redirect('admin');
  },
};

//handler for deleting selected user, finding the user, removing the users blogs and removing user.
exports.deleteUser = {

  handler: function (request, reply) {
    const data = request.payload;
    const userDelete = data.email;
    User.findOne({ email: data.email }).populate('blogs').then(user => {
      Tweet.find({ user: user._id }).then(blogs => {
        return Tweet.remove({ user: user._id });
      });
      User.find({}).then(users=> {
        for (let x of users) {
          x.follows.remove(user._id);
          x.followedBy.remove(user._id);
          x.save();
        }
      });
      User.findOne({ _id: user._id }).then(user=> {
        return User.remove(user);
      });
      reply.redirect('admin');
    }).catch(err => {
      reply.redirect('admin');
    });
  },
};

//handler for adminregister route, allowing admin to create users without leaving his view.
exports.adminRegister = {
  handler: function (request, reply, err) {
    User.findOne({ email: request.payload.email }).then(existingUser => {
      if (existingUser === null) {
        const user = new User(request.payload);
        user.save().then(newUser => {
          reply.redirect('/admin');
        });
      } else {
        throw err;
      }
    }).catch(err => {
      //I am aware that this causes a Error, Uncaught. But it is serving my purpose of triggering
      //ajax error function.
      throw err;
    });
  },

};

//handler for rendering admin stats view
exports.stats = {
  handler: function (request, reply) {
    let usersArray = [];
    User.find({}).populate('follows').populate('followedBy').then(updatedUsers => {
      for(let u of updatedUsers) {
        Tweet.find({ user: u._id }).then(tweets => {
          u.tweetNumber = Number(tweets.length);
          return u.save();
        });
        usersArray.push(u);
      }

      reply.view('adminstats', { users: usersArray });
    }).catch(err => {
      console.log(err);
      reply.view('admin');
    });
  },
};

