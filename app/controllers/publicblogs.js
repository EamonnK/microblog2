'use strict'
const User = require('../models/user');
const Tweet = require('../models/tweet');

//handler for public blog page, getting all blogs, rendering only posting users to dropdown
exports.publicblogs = {

  handler: function (request, reply) {
    let postingUser = [];
    let uniqueUsers = [];
    let current = request.auth.credentials.loggedInUser;
    User.findOne({ email: current }).then(user => {
      current = user;
    });
    Tweet.find({}).populate('user').then(allTweets => {
      for (let us of allTweets) {
        postingUser.push(us.user);
      }

      uniqueUsers = postingUser.filter(function (elem, index, self) {
        return index == self.indexOf(elem);
      });

      allTweets.reverse();
      reply.view('publicblogs', {
        title: 'Public Blogs',
        tweets: allTweets,
        users: uniqueUsers,
        user: current,
        current: current,
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//handler for select blog route, rendering specified user posts only along with dropdown only
//populated with posting users.
exports.selectBlogger = {

  handler: function (request, reply) {
    const data = request.payload;
    let current = request.auth.credentials.loggedInUser;
    User.findOne({ email: current }).then(user => {
      current = user;
    });
    if (data.user === '' || data.user === 'all bloggers') {
      reply.redirect('publicblogs');
    }else {
      User.findOne({ email: data.user }).then(selectedUser => {
        return Tweet.find({ user: selectedUser }).populate('user').then(userTweets => {
          let postingUser = [];
          Tweet.find({}).populate('user').then(tweets => {
            for (let us of tweets) {
              postingUser.push(us.user);
            }

            return postingUser;
          }).then(postingUser => {
            const uniqueUsers = postingUser.filter(function (elem, index, self) {
              return index == self.indexOf(elem);
            });

            userTweets.reverse();
            reply.view('publicblogs', {
              title: 'Public Blogs',
              tweets: userTweets,
              users: uniqueUsers,
              user: current,
              current: current,
            });
          });
        });
      }).catch(err => {
        reply.redirect('publicblogs');
      });
    }
  },
};

