'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  img: { data: Buffer, contentType: String },
  tweetNumber: { type: Number, default: 0 },
  follows: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  ],
  followedBy: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  ],
});

const User = mongoose.model('User', userSchema);
module.exports = User;
