'use strict'

//model for blog
const mongoose = require('mongoose');

const blogSchema = mongoose.Schema({
  content: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  img: { data: Buffer, contentType: String },
});

const Blog = mongoose.model('Blog', blogSchema);
module.exports = Blog;
