'use strict'

//model for blog
const mongoose = require('mongoose');

const tweetSchema = mongoose.Schema({
  tweetMessage: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  img: { data: Buffer, contentType: String },
  photo: String,
  base64String: String,
},
    {
  timestamps: true,
});

const Tweet = mongoose.model('Tweet', tweetSchema);
module.exports = Tweet;
