#Microblog2, Node.js#

### What is this repository ###

* This repository contains a twitter clone application
* Users can sign up/login, make tweets with images or without.
* Establish social relationships.
* Delete selected or all tweets made by them.
* Edit their own account settings.
* Upload a profile picture.
* View "Tweeters" profiles by clicking their avatar icon on the public timeline.
* View tweets of all users, or filter by dropdown menu.
* Admin can delete selected users, their tweets, has a statistics view showing users social stats.

### How to run ###

* The application can be run by typing node index.js on the command line.
* It can also be started via webstrom application
* Administrator login is admin@admin.com secret
* Sample user login is homer@simpson.com secret

## Hosted On ##
* https://microblogeamonn.herokuapp.com/