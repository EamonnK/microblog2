'use strict';

const assert = require('chai').assert;
const TweetService = require('./mytweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('User API tests', function () {

  let users = fixtures.users;
  let newUser = fixtures.newUser;

  const tweetService = new TweetService(fixtures.tweetService);

  beforeEach(function () {

    tweetService.login(users[0]);
    //tweetService.deleteAllUsers();

  });

  afterEach(function () {
   // tweetService.deleteAllUsers();
    tweetService.logout();

  });

  test('create a user', function () {
    const returnedUser = tweetService.createUser(newUser);
    returnedUser.password = newUser.password;
    assert(_.some([returnedUser], newUser), 'returnedUser must be a superset of newUser');
    assert.isDefined(returnedUser._id);
  });

  test('get User', function () {
    const c1 = tweetService.createUser(newUser);
    const c2 = tweetService.getUser(c1._id);
    c2.password = c1.password;
    assert.deepEqual(c1, c2);
  });

  test('get invalid user', function () {
    const c1 = tweetService.getUser('1234');
    assert.isNull(c1);
    const c2 = tweetService.getUser('012345678901234567890123');
    assert.isNull(c2);
  });

  test('delete a user', function () {
    const c = tweetService.createUser(newUser);
    assert(tweetService.getUser(c._id) != null);
    tweetService.deleteOneUser(c._id);
    assert(tweetService.getUser(c._id) == null);
  });

 /* test('get all users', function () {
    for (let u of users) {
      tweetService.createUser(u);
    }

    const allUsers = tweetService.getUsers();
    assert.equal(allUsers.length, users.length);
  });*/

  test('get users detail', function () {
    for (let u of users) {
      tweetService.createUser(u);
    }

    const allUsers = tweetService.getUsers();
    for (var i = 0; i < users.length; i++) {
      allUsers[i].password = users[i].password;
      assert(_.some([allUsers[i]], users[i]), 'returnedUser must be a superset of newUser');
    }
  });

  /*test('get all users empty', function () {
    const allUsers = tweetService.getUsers();
    assert.equal(allUsers.length, 0);
  });*/
});
