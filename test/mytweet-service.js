'use strict';

const SyncHttpService = require('./sync-http-service');
const baseUrl = 'http://localhost:4001';

class TweetService {

  constructor(baseUrl) {
    this.httpService = new SyncHttpService(baseUrl);
  }

  getTweets() {
    return this.httpService.get('/api/tweets');
  }

  getTweet(id) {
    return this.httpService.get('/api/tweets/' + id);
  }

  createTweet(id, newTweet) {
    return this.httpService.post('/api/tweets', newTweet, id);
  }

  deleteAllTweets() {
    return this.httpService.delete('/api/tweets');
  }

  follow(id, idSource) {
    return this.httpService.post('/api/users/' + id, idSource);
  }

  deleteOneTweet(id) {
    return this.httpService.delete('/api/tweets/' + id);
  }

  deleteUserTweets(id) {
    return this.httpService.delete('/api/users/' + id + '/tweets');
  }

  getUsers() {
    return this.httpService.get('/api/users');
  }

  getUser(id) {
    return this.httpService.get('/api/users/' + id);
  }

  createUser(newUser) {
    return this.httpService.post('/api/users', newUser);
  }

  deleteAllUsers() {
    return this.httpService.delete('/api/users');
  }

  deleteOneUser(id) {
    return this.httpService.delete('/api/users/' + id);
  }

  login(user) {
    return this.httpService.setAuth('/api/users/authenticate', user);
  }

  logout() {
    this.httpService.clearAuth();
  }
}

module.exports = TweetService;
