'use strict';

const assert = require('chai').assert;
const TweetService = require('./mytweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Tweets API tests', function () {

  let tweets = fixtures.tweets;
  let newUser = fixtures.newUser;
  let users = fixtures.users;

  const tweetService = new TweetService(fixtures.tweetService);

  beforeEach(function () {
    tweetService.login(users[0]);
    //tweetService.deleteAllUsers();
    tweetService.deleteAllTweets();
  });

  afterEach(function () {
    tweetService.deleteAllTweets();
    //tweetService.deleteAllUsers();
    tweetService.logout();
  });

  test('create a tweet', function () {
    const returnedUser = tweetService.createUser(newUser);
    tweetService.createTweet(returnedUser._id, tweets[0]);
    const returnedTweets = tweetService.getTweets(returnedUser._id);
    assert.equal(returnedTweets.length, 1);
    assert(_.some([returnedTweets[0]], tweets[0]), 'returned donation must be a superset of donation');
  });

  test('create multiple tweets', function () {
    const returnedUser = tweetService.createUser(newUser);
    for (var i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser._id, tweets[i]);
    }

    const returnedTweets = tweetService.getTweets(returnedUser._id);
    assert.equal(returnedTweets.length, tweets.length);
    for (var i = 0; i < tweets.length; i++) {
      assert(_.some([returnedTweets[i]], tweets[i]), 'returned donation must be a superset of donation');
    }
  });

  test('delete all tweets', function () {
    const returnedUser = tweetService.createUser(newUser);
    for (var i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser._id, tweets[i]);
    }

    const t1 = tweetService.getTweets(returnedUser._id);
    assert.equal(t1.length, tweets.length);
    tweetService.deleteAllTweets();
    const t2 = tweetService.getTweets(returnedUser._id);
    assert.equal(t2.length, 0);
  });

  test('delete user tweets', function () {
    const returnedUser = tweetService.createUser(newUser);
    for (var i = 0; i < tweets.length; i++) {
      tweetService.createTweet(returnedUser._id, tweets[i]);
    }
    const returnedTweets = tweetService.getTweets(returnedUser._id);
    assert.equal(returnedTweets.length, tweets.length);
    tweetService.deleteUserTweets(returnedUser._id);
    const returnedTweets1 = tweetService.getTweets(returnedUser._id);
    assert.equal(returnedTweets1.length, 0);
  });

  test('get invalid tweet', function () {
    const c1 = tweetService.getTweet('1234');
    assert.isNull(c1);
    const c2 = tweetService.getTweet('012345678901234567890123');
    assert.isNull(c2);
  });
});
