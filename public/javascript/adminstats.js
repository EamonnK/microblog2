//Arrays for rendering charts
var tweetNames  = new Array();
var tweetTotals = new Array();
var followsTotals = new Array();
var followedByTotals = new Array();
var followsNames = new Array();
var userTweetTotals = new Array();
var followsPercentages = new Array();
var followedByPercentages = new Array();
var totalFollows = 0;
var totalFollowedBy = 0;
var total = 0;

$(document).ready(function () {
  getArrays();
  render(tweetNames, tweetTotals, followsTotals, followsNames, userTweetTotals, followedByTotals, followsPercentages, followedByPercentages, followsTotals, followedByTotals);
});

/**
 *Function  to get arrays of users, tweets and follow/edBy stats
 */
function getArrays() {
  var i = 0;

  $('.tweetName').each(function () {
    tweetNames[i] = $(this).attr('value');
    i++;
  });

  i = 0;
  $('.tweetTotal').each(function () {
    total = Number($(this).attr('value')) + total;
    i++;
  });

  i = 0;
  $('.tweetTotal').each(function () {
    tweetTotals[i] = Number($(this).attr('value')) * 100 / total;
    userTweetTotals.push(Number($(this).attr('value')));
    i++;
  });

  i = 0;
  $('.numberFollows').each(function () {
    followsTotals[i] = Number($(this).attr('value'));
    totalFollows = Number($(this).attr('value')) + totalFollows;
    i++;
  });

  i = 0;
  $('.numberFollowedBy').each(function () {
    followedByTotals[i] = Number($(this).attr('value'));
    totalFollowedBy = Number($(this).attr('value')) + totalFollowedBy;
    i++;
  });

  i = 0;
  $('.numberFollows').each(function () {
    followsPercentages[i] = Number($(this).attr('value')) * 100 / totalFollows;
    i++;
  });

  i = 0;
  $('.numberFollowedBy').each(function () {
    followedByPercentages[i] = Number($(this).attr('value')) * 100 / totalFollowedBy;
    i++;
  });

  i = 0;
  $('.numberNames').each(function () {
    followsNames[i] = $(this).attr('value');
    i++;
  });

}

/**
 * Function to render charts with array data
 *
 * @param tweetNames
 * @param tweetTotals
 * @param followsTotals
 * @param followsNames
 * @param userTweetTotals
 * @param followedByTotals
 * @param followsPercentages
 * @param followedByPercentages
 */
function render(tweetNames, tweetTotals, followsTotals, followsNames, userTweetTotals, followedByTotals, followsPercentages, followedByPercentages) {
  let dps = dataArray(tweetNames, tweetTotals);

  $('#chartContainer').CanvasJSChart({

    title: {
      text: 'Tweeting %',
      fontColor: 'white',
      fontSize: 24,
    },
    legend: {
      verticalAlign: 'bottom',
      horizontalAlign: 'center',
    },
    data: [{
      type: 'pie',
      showInLegend: true,
      toolTipContent: '{label} <br/> {y} %',
      indexLabel: '{y} %',
      dataPoints: dps,
    }, ],
  });

  let dps0 = dataArray(tweetNames, userTweetTotals);
  $('#tweetBarContainer').CanvasJSChart({

    title: {
      text: 'Tweeting Numbers',
      fontColor: 'white',
      fontSize: 24,
    },
    axisX: {
      labelFontColor: 'white',
    },
    axisY: {
      labelFontColor: 'white',
    },
    data: [{
      type: 'bar',
      toolTipContent: '{label} <br/> {y} ',
      indexLabel: '{y} Tweets ',
      indexLabelPlacement: 'inside',
      dataPoints: dps0,
    }, ],
  });

  let dps1 = dataArray(followsNames, followsTotals);
  $('#barContainer').CanvasJSChart({

    title: {
      text: 'Follows Stats',
      fontColor: 'white',
      fontSize: 24,
    },
    axisX: {
      labelFontColor: 'white',
    },
    axisY: {
      labelFontColor: 'white',
    },
    data: [{
      type: 'bar',
      toolTipContent: '{label} <br/> {y} ',
      indexLabel: 'Following {y}',
      indexLabelPlacement: 'inside',
      dataPoints: dps1,
    }, ],
  });

  let dps2 = dataArray(followsNames, followedByTotals);
  $('#followedByBarContainer').CanvasJSChart({

    title: {
      text: 'Following Stats',
      fontColor: 'white',
      fontSize: 24,
    },
    axisX: {
      labelFontColor: 'white',
    },
    axisY: {
      labelFontColor: 'white',
      labelAutoFit: 'true',
    },
    data: [{
      type: 'bar',
      toolTipContent: '{label} <br/> {y} ',
      indexLabel: 'Followed By {y}',
      indexLabelPlacement: 'inside',
      dataPoints: dps2,
    }, ],
  });

  let dps3 = dataArray(followsNames, followsPercentages);
  $('#followsPie').CanvasJSChart({

    title: {
      text: 'Follows %',
      fontColor: 'white',
      fontSize: 24,
    },
    legend: {
      verticalAlign: 'bottom',
      horizontalAlign: 'center',
    },
    data: [{
      type: 'pie',
      showInLegend: true,
      toolTipContent: '{label} <br/> {y} %',
      indexLabel: '{y} %',
      dataPoints: dps3,
    }, ],
  });

  let dps4 = dataArray(followsNames, followedByPercentages);
  $('#followedByPie').CanvasJSChart({

    title: {
      text: 'Followed By %',
      fontColor: 'white',
      fontSize: 24,
    },
    legend: {
      verticalAlign: 'bottom',
      horizontalAlign: 'center',
    },
    data: [{
      type: 'pie',
      showInLegend: true,
      toolTipContent: '{label} <br/> {y} %',
      indexLabel: ' {y} %',
      dataPoints: dps4,
    }, ],
  });
}

/**
 * Function to format data from arrays to number,
 * filling required fields for chart. called above.
 * @param tweetNames
 * @param tweetTotals
 * @returns {Array}
 */

function dataArray(tweetNames, tweetTotals) {
  let data = [];
  for (let i = 0; i < tweetNames.length; i += 1) {
    data.push({ label: tweetNames[i],
      y: Number(Math.round((tweetTotals[i]) * 10) / 10),
      legendText: tweetNames[i], });
  }

  return data;
}

