//global variable for setting stat on cards, was necessary to make consistently accurate.
let number = document.getElementById('numberFollows').getAttribute('value');

//ajax call to follow
$('.ui.form.follow')
    .form({
      onSuccess: function (event) {
        follow();
        event.preventDefault();
      },
    });

//Ajax call for follow
function follow() {
  var formData = $('.ui.form.follow input').serialize();
  $.ajax({
    type: 'POST',
    url: '/follow',
    data: formData,
    success: function (response) {
      let name = document.getElementById('follower').getAttribute('value');
      let id = document.getElementById('source').getAttribute('value');
      addFollower(name, id, number);
    },
  });
};

//ajax call to unfollow
$('.ui.form.unfollow')
    .form({
      onSuccess: function (event) {
        unfollow();
        event.preventDefault();
      },
    });

//Ajax call for delete posts route
function unfollow() {
  var formData = $('.ui.form.unfollow input').serialize();
  $.ajax({
    type: 'POST',
    url: '/unfollow',
    data: formData,
    success: function (response) {
      let id = document.getElementById('source').getAttribute('value');
      unFollow(id);
    },
  });
};

//function to add card once user follows alos adjust for statistic
function addFollower(name, id, number) {
  let newNumber = Number(number) + 1;
  let $div =  $(/*'<div class="newfollower">' +*/
      '<form action="/getprofile" method="POST"  class="newfollower">' +
      '<button type="submit" class="center aligned header" style="color:white; background-color: rgba(0,0,0,.0);border:none;outline:none">' +
      '<input value="' + id + '" name="_id" class="foll" hidden>' +
      '<div class="ui card" style="width:100%; background-color: rgba(0,0,0,.8); value="' + id + '"> ' +
      '<div class="image" > ' +
      '<img class="ui image" style="border:5px double silver; height:60%; width:70%; border-radius: 100px; opacity: .9; margin-left:15%" onerror="this.src= $(this).attr(' + '\'altSrc\'' + ')" altSrc="/images/avatar.jpg" src="/getImage/' + id + '" > ' +
      '</div> ' +
      '<div class="content">' +
      '<div  class="center aligned header" style="color:white">' + name + '</div>' +
      '</div>' +
      '<div class="center aligned extra content" id="number" style="color:white"> Following:' + newNumber + '</div> ' +
      '</div>' +
      '</button>' +
      '</form>');

  //setting listner to allow for element to be removed without page refresh
  $(document).on('click', '#unfollowButton', function () {
    $('.newfollower').remove();
  });

  $('#followedBy').append($div);
}

//function to remove card and reformat number on unfollow
function unFollow(id) {
  $('.card').each(function () {
    if (this.getAttribute('value') === id) {
      let x = Number($(this).find('h4').text()) - 1;
      $(this).find('h4').text(x);
      number = x;
      this.remove();
    }
  });
};

//onload function to execute various tasks on page load.
window.onload = function start() {
  let current = document.getElementById('source').getAttribute('value');
  let yourPage = document.getElementById('target').getAttribute('value');

  //function too format date for tweets
  $('.center.aligned.date.header').each(function () {
    let shorter = this.innerHTML.substring(0, 28);
    this.innerHTML = shorter;
  });

  //function to switch buttons if follow relationship already exists
  $('.foll').each(function () {
    console.log('value :' + this.getAttribute('value'));
    if (this.getAttribute('value') === current) {
      console.log('gettingiin:  :' + this.getAttribute('value'));
      document.getElementById('followButton').style.display = 'none';
      document.getElementById('unfollowButton').style.display = 'block';
    }
  });

  //hide follow button if current user is viewing their own profile
  if (yourPage === current) {
    document.getElementById('followButton').style.display = 'none';
    document.getElementById('unfollowButton').style.display = 'none';
  }
};

//function to switch displayed button
function check() {
  document.getElementById('followButton').style.display = 'none';
  document.getElementById('unfollowButton').style.display = 'block';
}

//function to switch displayed button
function uncheck() {
  document.getElementById('followButton').style.display = 'block';
  document.getElementById('unfollowButton').style.display = 'none';
}

