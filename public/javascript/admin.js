//form validation for register User on admin page.
$('.ui.form.signup')
    .form({
      fields: {
        firstName: {
          identifier: 'firstName',
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter your first name.',
            },
          ],
        },

        lastName: {
          identifier: 'lastName',
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter your last name.',
            },
          ],
        },

        address: {
          identifier: 'address',
          rules: [
            {
              type: 'empty',
              prompt: 'Please enter your address.',
            },
          ],
        },

        email: {
          identifier: 'email',
          rules: [
            {
              type: 'email',
              prompt: 'Please enter a valid email address.',

            },
          ],
        },

        password: {
          identifier: 'password',
          rules: [
            {
              type: 'minLength[6]',
              prompt: 'Please enter password(6 character minimum).',
            },
          ],
        },
      },
      onSuccess: function (event) {
        registerUser();
        event.preventDefault();
      },
    });

//Ajax call for adminRegister user route
function registerUser() {
  var formData = $('.ui.form.signup input').serialize();
  $.ajax({
    type: 'POST',
    url: '/adminRegister',
    data: formData,
    success:
        function () {
          //getting newly registered email
          email = document.getElementById('email').value;

          //creating new div item for drop down with user append function
          newUser = userAppend(email);

          //adding new email with value to delete dropdown
          $('.menu.delete').append(newUser);

          //resetting form and hiding it again
          document.getElementById('signform').reset();
          document.getElementById('signUp').style.display = 'none';
          document.getElementById('existing').style.display = 'none';

        },
    error:
     function(){
       document.getElementById('signform').reset();
       document.getElementById('existing').style.display = 'block';
     }
  });
};

//funciton to create a div for appending to dropdown
function userAppend(email) {
  return '<div class="item blogger"' + ' ' + 'value="' + email + '"' + ' ' +
      'style="background-color: rgba(0,0,0,.8); color:white">' + email + '</div>';
}

//form validation for deleteUser
$('.ui.delete.form')
    .form({
      fields: {
        email: {
          identifier: 'email',
          rules: [
            {
              type: 'empty',
              prompt: 'You must select a user to delete.',
            },
          ],
        },
      },
      onSuccess: function (event, fields) {
        deleteUser();
        event.preventDefault();
      },
    });

//Ajax call for delete user route
function deleteUser() {
  var formData = $('.ui.delete.form input').serialize();
  $.ajax({
    type: 'POST',
    url: '/deleteUser',
    data: formData,
    success:
        function (response) {

          //getting selected user email from dropdown
          let email = $('#DeleteUser').dropdown('get value');

          //removing user from filter blogs dropdown
          removeUserFilter(email);

          //removing user from delete dropdown
          removeUserDelete(email);

          //removing table entries from that user
          cleanTable(email);
        },
  });
};

//form validaiton for filter user dropdown
$('.ui.filter.form')
    .form({
      fields: {
        email: {
          identifier: 'email',
          rules: [
            {
              type: 'empty',
              prompt: 'You must select a user to filter.',
            },
          ],
        },
      },
      onSuccess: function (event, fields) {
        let email = $('#FilterUser').dropdown('get value');
        selectTable(email);
        event.preventDefault();
      },
    });

//Have left this in to show that it was implement, Ajax for filter user blogs
//however i found this responds much faster with jquery, which removed the need for this,
//the export handler and route for admin filter.
/*function filterUser() {
  var formData = $('.ui.filter.form input').serialize();
  $.ajax({
    type: 'POST',
    url: '/selectUserBlogs',
    data: formData,
    success:
        function (response) {
          let email = $('#FilterUser').dropdown('get value');
          selectTable(email);
        },
  });
};*/

//delete post form 
$('.ui.delblog.form')
    .form({
      fields: {

      },
      onSuccess: function (event) {
        deleteBlogs();
        deleteChecked();
        cleanFilter();
        event.preventDefault();
      },
    });

//Ajax call for delete posts route
function deleteBlogs() {
  var formData = $('input[type="checkbox"]:checked').serialize();;
  $.ajax({
    type: 'POST',
    url: '/deleteSelectedBlogs',
    data: formData,
  });
};

//function to remove user from delete dropdown
function removeUserDelete(email) {
  let $obj = $('.item.blogger');
  for (let i = 0; i < $obj.length; i += 1) {
    if ($obj[i].getAttribute('value').toLowerCase().localeCompare(email.toLowerCase()) == 0) {
      $obj[i].remove();
      $('#DeleteUser').dropdown('clear');
      break;
    }
  }
};

//function to remove user from filter dropdown
function removeUserFilter(email) {
  let $obj = $('.item.bloggers');
  for (let i = 0; i < $obj.length; i += 1) {
    if ($obj[i].getAttribute('value').toLowerCase().localeCompare(email.toLowerCase()) == 0) {
      $obj[i].remove();
      $('#FilterUser').dropdown('clear');
      break;
    }
  }
};

//function to remove user posts from table upon deletion of the user
function cleanTable(email) {
  $('.usersBlogs').each(function () {
    if (this.getAttribute('value') === email) {
      this.remove();
    }
  });
};

//function replacing export select posts, filtering user posts on page.
//much better response time.
function selectTable(email) {
  $('.usersBlogs').each(function () {
    $(':checkbox').each(function () {
        this.checked = false;
      });

    if (email === 'all bloggers') {
      this.style.display = '';
    } else {
      if (this.getAttribute('value') === email) {
        if (this.style.display === 'none') {
          this.style.display = '';
        }
      } else {
        this.style.display = 'none';
      }
    }
  });
};

//function to remove the table row conatining the selected checkbox
function deleteChecked() {
  $('.usersBlogs').each(function () {
    $('input[type="checkbox"]:checked').parents('tr').remove();
  });
};

//function to clean the filter dropdown menu upon deletion of all of a users posts
function cleanFilter() {
  let emails = [];
  $('.usersBlogs').each(function () {
    emails.push(this.getAttribute('value'));
  });

  $('.item.bloggers').each(function () {
    let email = this.getAttribute('value');
    let i = 0;
    for (let x of emails) {
      if (x === email) {
        i += 1;
      }
    }

    if (i === 0) {
      removeUserFilter(email);
    }
  });
}

//function to make signup appear on admin page....as if from nowhere!!
function peekaBoo() {
  let check = document.getElementById('signUp');
  if (check.style.display === 'none') {
    check.style.display = 'block';
  }else {
    document.getElementById('existing').style.display = 'none';
    check.style.display = 'none';
  }
}

//function to format date on tweets
window.onload = function date() {

  $('.center.aligned.date.header').each(function () {
    let shorter = this.innerHTML.substring(0, 28);
    this.innerHTML = shorter;
  });
}

//function to select all tweets, allowing for a few and then all to be selected or unselected
function selectAll() {
  let i = 0;
  let x = 0;
  $(':checkbox').each(function () {
    x += 1;
    i += (this.checked === false) ? 0 : 1;
  });

  $(':checkbox').each(function () {
    var row = $(this).closest('tr');
    if (row[0].style.display === '') {
      this.checked = (i < x) ? true : false;
    }
  });
}

