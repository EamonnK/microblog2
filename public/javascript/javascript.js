'use strict'

//function to implement coutner on users post blog page
function textCounter(field, field2, maxlimit) {
  var countfield = document.getElementById(field2);
  if (field.value.length > maxlimit) {
    field.value = field.value.substring(0, maxlimit);
    return false;
  } else {
    countfield.value = maxlimit - field.value.length;
  }
}

//function to select all checkboxes when select is clicked
//added functionality to check if div is visible and if not dont check it
function selectAll() {
  let i = 0;
  let x = 0;
  $(':checkbox').each(function () {
    x += 1;
    i += (this.checked === false) ? 0 : 1;
  });

  $(':checkbox').each(function () {
    var row = $(this).closest('tr');
    if (row[0].style.display === '') {
      this.checked = (i < x) ? true : false;
    }
  });
}

//onload funtion formatting tweet date for display
window.onload = function date() {
  $('.center.aligned.date.header').each(function () {
    let shorter = this.innerHTML.substring(0, 28);
    this.innerHTML = shorter;
  });
}
